// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebaseConfig: {
    apiKey: "AIzaSyCCbqRWSHs_OV8MrQhhz3EHy7TdeOC7Z0c",
    authDomain: "prototype-65963.firebaseapp.com",
    databaseURL: "https://prototype-65963.firebaseio.com",
    projectId: "prototype-65963",
    storageBucket: "prototype-65963.appspot.com",
    messagingSenderId: "509174315558",
    appId: "1:509174315558:web:a6e1fd67fb42bf252404e7"
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
