import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {AngularFireAuth} from '@angular/fire/auth';
import { HomePage } from '../app/home/home.page';
import { LoginPage } from '../app/login/login.page';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  rootPage:any = LoginPage;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/company-list',
      icon: 'list'
    },
    {
      title: 'Login',
      url: '/login',
      icon: 'home'
    },
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public afAuth: AngularFireAuth,
  ) {
    this.initializeApp();
    const authObserver = afAuth.authState.subscribe(user => {
      if (user) {
          this.rootPage = HomePage;
          authObserver.unsubscribe();
      } else {
          this.rootPage = LoginPage;
          authObserver.unsubscribe();
      }
  });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  
}
