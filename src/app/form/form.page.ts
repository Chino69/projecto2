import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl, RequiredValidator } from '@angular/forms';

import { PhoneValidator } from '../validators/phone.validator';
import { PasswordValidator } from '../validators/password.validator';
import { CountryPhone } from './country-phone.model';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import {DomSanitizer} from '@angular/platform-browser'
import { Base64 } from '@ionic-native/base64/ngx';
@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit {

  info: JSON;
  validations_form: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;

  countries: Array<CountryPhone>;
  genders: Array<string>;
  positions: Array<string>;
  companies: Array<any>;
  users: Array<any>;
  image: any;

  //public place$: Observable<values.position[]>
  

  constructor(
    
    public formBuilder: FormBuilder,
    private router: Router,
    public imagePicker: ImagePicker,
    public file: File,
    private base64: Base64,
    public sanitizer: DomSanitizer
    
  ) { }

  ngOnInit() {
    //  We just use a few random countries, however, you can use the countries you need by just adding them to this list.
    // also you can use a library to get all the countries from the world.
    
    this.countries = [
      new CountryPhone('CR', 'Costa Rica'),
      
    ];

    this.genders = [
      "Male",
      "Female"
    ];
    this.positions = [
      "Company",
      "User"
    ];
    
    

    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });

    let country = new FormControl(this.countries[0], Validators.required);
    let phone = new FormControl('', Validators.compose([
      Validators.required,
      PhoneValidator.validCountryPhone(country)
    ]));
    this.country_phone_group = new FormGroup({
      country: country,
      phone: phone
    });

    this.validations_form = this.formBuilder.group({
      
      name: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      cedula: new FormControl('', Validators.required),//Validators.compose([
        //Validators.required,
       // Validators.pattern('^[1-9]-[0-9]{4,4}-[0-9]{4,4}$'),
        

    
      curriculum: new FormControl('', ),
      gender: new FormControl(this.genders[0], Validators.required),
      
      //no se si se puede pero quiero PODER LLAMAR ESTA VARA EN EL HTML PARA USARLA EN EL NGIF PERO O NO SE PUEDE O SOY DEMASIDO
      //inútil y no se como se hace. Para que cuando digan que son una compañía, le salga la opción para poner más información.
      position: new FormControl("", Validators.required),
      
      
      

      country_phone: this.country_phone_group,
      matching_passwords: this.matching_passwords_group,
      extra_info: new FormControl('', Validators.required),
      
      terms: new FormControl(true, Validators.pattern('true')),
      image: new FormControl("", )
      
    });
  }
  
  

  validation_messages = {
    
    'name': [
      { type: 'required', message: 'Su nombre es requerido.' }
    ],
    'lastname': [
      { type: 'required', message: 'Su apellido es requerido.' }
    ],
    'extra_info': [
      {type: 'required', message: 'La información extra es requerida.'}
    ],
    'position': [
      { type: 'required', message: 'La posición es requerida.' }
    ],
    
    'email': [
      { type: 'required', message: 'Su correo electrónico es requerido.' },
      { type: 'pattern', message: 'Por favor ingrese un correo electrónico válido.' }
    ],
    'phone': [
      { type: 'required', message: 'Su número de teléfono es requerido.' },
      { type: 'validCountryPhone', message: 'El número de teléfono ingresado es inválido.' }
    ],
    'password': [
      { type: 'required', message: 'La contraseña es requerida.' },
      { type: 'minlength', message: 'La contraseña debe contener 5 caracteres mínimo.' },
      { type: 'pattern', message: ' La contraseña de contener al menos una mayúscula, una minúscula, y un número.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'La confirmación de la contraseña es requerida.' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Por favor verifique que las contraseñas sean iguales.' }
    ],
    'terms': [
      { type: 'pattern', message: 'Debe aceptar los términos y condiciones.' }
    ],
    'cedula': [
      {type: 'required', message: 'El número de cédula es requerido.' }
    ]
  };

  async openImagePicker(){
    await this.imagePicker.hasReadPermission()
    .then(async (result) => {
      if(result == false){
        // no callbacks required as                    this opens a popup which returns async
        this.imagePicker.requestReadPermission();
      }
      else if(result == true){
      console.log("Ya tiene permisos Eso!");
      await this.imagePicker.getPictures({
       maximumImagesCount: 1
      }).then(
      async (results) => {
        for (var i = 0; i < results.length; i++) {
          const aux = this;

         const element = results[i];
          
          let filePath: string = '';
          this.base64.encodeFile(element).then( async (base64File: string) => {
           console.log(base64File);
            this.image = this.sanitizer.bypassSecurityTrustUrl(base64File);
            console.log(this.image);
            console.log('Despues sigue firebase');
            
           console.log('NO FUE FIREBASE');
          }, (err) => {
            console.log('ERROR PRIMERO')
            
          });  
          console.log('no se imagenes')
        console.log(results)
        
        }
               }, (err) => console.log('ERROR SEGUNDO', )
     );
      }
    }, (err) => {
      console.log(' ERROR, ultimo')
    });
  }

  onSubmit(values){
    console.log(values);
    this.info = values;
    values.image = this.image
    if(values.position==="User") {firebase.database().ref('users/').push(values);
    
  } else {
    firebase.database().ref('companies/').push(values)
    
  }
  
    this.router.navigate(["/home"]);
  }

}
