import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
//import {AngularFireDatabase} from '@angular/fire/database';
import { IonicModule } from '@ionic/angular';

import { CompanyListPage } from './company-list.page';

const routes: Routes = [
  {
    path: '',
    component: CompanyListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
   // AngularFireDatabase,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CompanyListPage]
})
export class CompanyListPageModule {}
