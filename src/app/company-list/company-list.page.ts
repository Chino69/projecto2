
import { Component, OnInit, ViewChild } from '@angular/core';

import * as firebase from "firebase";
import { AngularFireDatabase} from '@angular/fire/database';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import {NavController} from '@ionic/angular';
import { Action } from 'rxjs/internal/scheduler/Action';
@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.page.html',
  styleUrls: ['./company-list.page.scss'],
})
export class CompanyListPage  {
  //ref = firebase.database().ref('/users');
  info: {}
  hola = 1;
  companiesRef: any;
  companies: any;
  _companies: Observable<any[]>;
  companyKeys: any[];

  constructor(
    public router: Router, 
    private route: ActivatedRoute, 
    public navCtrl: NavController,
    public db: AngularFireDatabase){firebase.database().ref('companies/'+this.route.snapshot.paramMap.get('id')).on('value', snapshot => {
      if(snapshot.val()){
        console.log(snapshot.val())
      
    
        this.info = snapshot.val()
      
        console.log(this.info)}
    });
      this.companiesRef = db.list('companies');
      this._companies = this.companiesRef.valueChanges();

      //function buscar(termino: string){
       // console.log(termino)
     // }
      console.log("***************");
      console.log("Las companias");
      console.log(this._companies);
      console.log("***************");

      this.companiesRef.snapshotChanges(['child_added'])
        .subscribe(actions => {
          actions.forEach(action =>  {
            console.log(action.type);
            console.log(action.key);
            
            console.log(action.payload.val());
          });
        });
       
    }
    
    
     companyDesc(key){
      this.router.navigate(['/company-desc/'+key])
      // this.companiesRef.auditTrail().subscribe(actions =>{
      //  console.log(actions);
      //   actions.forEach(action => {
      //     this.navCtrl.navigateForward(['/company-desc/'+ action.key])
      //   })
      // })
      
     } 
    
    ionViewWillEnter() {
      //executes when screen is uploaded

    }
}